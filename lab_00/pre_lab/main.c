#include <stdio.h>
#include <stdint.h>

uint32_t bit_merge(uint16_t n1, uint16_t n2){
  return (uint32_t) n1 << 16 | n2; // Precedence Table FTW ^^
}


int16_t f1(int16_t j){
  j = 5;
  return j*j;
}

int16_t f2(int16_t*i){
  *i = 6;
  return*i;
}

void main(){
  int16_t i=0;
  int16_t j=1;
  int16_t k,l;
  k = f1(i);
  l = f2(&j)+ k;
  printf("%d %d %d %d", i, j, k, l);
}