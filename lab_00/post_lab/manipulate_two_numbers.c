// lab00, lab procesure, task 4

// source file for the program manipulate_two_numbers

/*
 * This file is used to generate an executable program that reads two
 * numbers from the terminal and output the merged result in 
 * hexadecimal, and the sum in hexadecimal and binary format.
*/

// again, the header of our function library is already included 
#include "functions.h"

// main
int main(int argc, char *argv[])
{
	uint16_t var1, var2;
	uint32_t sum, merge;

	printf("Please enter two numbers separated by one comma, without blank space \n");

	while(1){
		scanf("%x,%x",&var1,&var2);

		merge = bit_merge(var1, var2);
		sum = var1+var2;

		if(var2 == 0 && var1 == 0){
			break;
		}

		printf("merging  0x%x and 0x%x  results in 0x%x \n", var1, var2, merge);
		printf("the sum is in hex 0x%x, bin: ", sum);
		print_bits(sum);
		printf("\n\n");
	}
	return 0;
}
