// lab00, lap procedure, task 2

// source file for the program sum_numbers

/*
 * This file is used to generate an executable program that prints the 
 * sum of the two numbers in binary and hexadecimal format.
*/

// Since we will be using our own functions, we need to add the header
// file where the functions are declared.
// Keep in mind that the header file already includes standard libraries
// so they do not to be included here.
#include "functions.h"
 
 // main function
int main(int argc, char *argv[])
{
	//input from the console
	uint16_t var1, var2;
	scanf("%x,%x",&var1,&var2);

	//add the numbers	
	uint32_t res = var1+var2;

	// use the function print_bits
	printf("0b ");
	print_bits(res);
	printf("\n 0x %x", res);

	return 0;
}
