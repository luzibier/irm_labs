#include <Servo.h>

// declare variables

int poti_pin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin

// create a servo object to control the servo
Servo servo;

void setup() {
  
  // _______________ Begin - Setup _______________
  pinMode(poti_pin, INPUT);

  servo.attach(9, 550, 2300);

  Serial.begin(115200);
  // _______________ End - Setup _______________
  
}

void loop() {
  // _______________ Begin - Loop _______________

  voltage = analogRead(poti_pin); //read value from potentimeter

  angle = map(voltage, 0, 1023, 0, 180); //convert the voltage to angle

  servo.write(angle); //set the servo to the angle
  // _______________ End - Loop _______________
  
  delay(15); // wait for the servo to get there
}
